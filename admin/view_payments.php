<?php
if (!isset($_SESSION['user_email'])) {
    header('location: login.php?not_admin=You are not Admin!');
}


?>
<div class="row">
    <div class="col-sm-12">
        <h1>Customers</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <!-- <th scope="col">Ekspedisi</th> -->
                    <th scope="col">Total Harga</th>
                    <th scope="col">No Rek</th>
                    <!-- <th scope="col">Transfer</th> -->
                    <th scope="col">Image</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $get_cust = "select * from payment ORDER BY pay_id DESC";
                $run_cust = mysqli_query($con, $get_cust);
                $count_cust = mysqli_num_rows($run_cust);
                if ($count_cust == 0) {
                    echo "<h2> No Customer found </h2>";
                } else {
                    $i = 0;
                    while ($row_cust = mysqli_fetch_array($run_cust)) {
                        $cust_id = $row_cust['pay_id'];
                        $cust_name = $row_cust['pay_first'];
                        $cust_atas = $row_cust['pay_atas_nama'];
                        $cust_email = $row_cust['pay_email'];
                        $cust_ekspedisi = $row_cust['pay_ekspedisi'];
                        $cust_total = $row_cust['pay_total'];
                        $cust_norek = $row_cust['pay_norek'];
                        $cust_transfer = $row_cust['pay_transfer'];
                        $cust_image = $row_cust['pay_image'];
                        $cust_status = $row_cust['pay_status'];
                        if ($cust_status == 1) {
                            $cust_status = "<td class='btn btn-success'>Approved</td>";
                        } else if ($cust_status == -1) {
                            $cust_status = "<td class='btn btn-danger'>Rejected</td>";
                        } else if ($cust_status == 0) {
                            $cust_status = "<td class='btn btn-primary'>Waiting</td>";
                        }
                ?>
                        <tr>
                            <th scope="row"><?php echo ++$i; ?></th>
                            <td><?= $cust_atas; ?></td>
                            <td><?php echo $cust_email; ?></td>
                            <!-- <td><?= $cust_ekspedisi; ?></td> -->
                            <td>Rp. <?= number_format($cust_total, 2, ',', '.'); ?></td>
                            <td><?= $cust_norek; ?></td>
                            <!-- <td><?= $cust_transfer; ?></td> -->
                            <td> <?= '<a href="#" onclick="clickthis(\'' . $cust_image . '\')"><img src="../gallery/' . $cust_image . '" alt="bukti transfer" width="80" height="80"></a>'; ?>
                                <!-- <img class="img-thumbnail" src='../gallery/<?php echo $cust_image; ?>' width='80' height='80'> -->
                            </td>
                            <?= $cust_status; ?>
                            <td>
                                <a href="index.php?app_customer=<?php echo $cust_id ?>" class="btn btn-success">
                                    <i class=" fa fa-check"></i>
                                </a>
                                <a href="index.php?del_customer=<?php echo $cust_id ?>" class="btn btn-danger">
                                    <i class=" fa fa-times"></i>
                                </a>
                            </td>
                        </tr>
                <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img class="img-fluid" id="imgModal">
            </div>
        </div>
    </div>
</div>

<script>
    function clickthis(img) {
        $('#myModal').modal('show')
        $('#imgModal').attr('src', '../gallery/' + img);
    }
</script>