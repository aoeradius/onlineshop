-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: store_db
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `user_email` varchar(255) NOT NULL,
  `user_pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES ('aoeradiusam@gmail.com','463326');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `brand_id` int(100) NOT NULL AUTO_INCREMENT,
  `brand_title` varchar(255) NOT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'HP'),(2,'DELL'),(3,'Apple'),(4,'Samsung'),(5,'Sony'),(6,'Oppo'),(7,'Lenovo'),(8,'Toshiba'),(11,'Asus');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `p_id` int(10) NOT NULL,
  `ip_add` varchar(255) NOT NULL,
  `qty` int(10) NOT NULL DEFAULT 1,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (5,'::1',1),(14,'::1',1),(16,'::1',1);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `cat_id` int(100) NOT NULL AUTO_INCREMENT,
  `cat_title` varchar(100) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Laptop'),(2,'Camera'),(3,'Handphone'),(4,'Computer'),(5,'Tablet'),(6,'Ipad'),(7,'Jam Tangan');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `cust_id` int(10) NOT NULL AUTO_INCREMENT,
  `cust_ip` varchar(255) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `cust_email` varchar(100) NOT NULL,
  `cust_pass` varchar(100) NOT NULL,
  `cust_country` varchar(100) NOT NULL,
  `cust_city` varchar(100) NOT NULL,
  `cust_contact` varchar(100) NOT NULL,
  `cust_address` varchar(255) NOT NULL,
  `cust_image` varchar(200) NOT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (6,'::1','vandi','aoeradius@gmail.com','a463326','Select a Country','Kota Tangerang Selatan','085155479432','Rawamekar jaya','hqdefault.jpg');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_first` varchar(20) DEFAULT NULL,
  `pay_alamat` text DEFAULT NULL,
  `pay_email` varchar(100) DEFAULT NULL,
  `pay_ekspedisi` enum('jne','jnt') DEFAULT NULL,
  `pay_total` float DEFAULT NULL,
  `pay_norek` varchar(20) DEFAULT NULL,
  `pay_atas_nama` varchar(50) DEFAULT NULL,
  `pay_transfer` enum('paypal','bca','bni') DEFAULT NULL,
  `pay_image` varchar(255) DEFAULT NULL,
  `pay_status` int(1) DEFAULT NULL,
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` VALUES (1,'asd','asd','asd','jne',10347000,'paypal.me/kenzie1212','Irvandi','paypal','html.png',0),(2,'Shelby','shelby','shelby@kepo.co','jne',19297000,'4445666772','Risma Handayani','bca','shelby_prt13.png',0),(3,'asd','asd','asd','jne',NULL,'paypal.me/kenzie1212','Irvandi','paypal',NULL,0);
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `pro_id` int(100) NOT NULL AUTO_INCREMENT,
  `pro_cat` int(100) NOT NULL,
  `pro_brand` int(100) NOT NULL,
  `pro_title` varchar(250) NOT NULL,
  `pro_price` int(100) NOT NULL,
  `pro_desc` varchar(3000) NOT NULL,
  `pro_image` varchar(300) NOT NULL,
  `pro_keywords` varchar(300) NOT NULL,
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (5,4,3,'Komputer Gaming Intel I7',8950000,'Processor Intel I7 dan 1 set keyboard & mouse gaming','komputergaming.jpg','Komputer , Gaming , Murah'),(6,4,3,'imac',5890000,'big mac book','71KyjhnQY4L._SY355_.jpg','mac book, apple'),(7,1,2,'Dell Laptop Intel core 7',5790000,'Kapasitas: Ram 8GB | SSD 256GB\r\nDidukung oleh Generasi ke 7 Intel Core i5-7200U Prosesor seluler, kinerja pemrosesan dual-core yang cerdas untuk komputasi berkualitas HD. 15. 6 \"Led layar sentuh backlit dengan True-life HD. Memungkinkan Anda menikmati film, acara, dan permainan favorit Anda dalam resolusi 1366 x 768 yang memukau.\r\nMemori DDR4 8GB tersedia untuk menjalankan game, program, dan lainnya. SSD 256GB untuk start-up cepat sementara masih memiliki banyak ruang penyimpanan untuk dokumen dan program Anda.\r\nIntel HD Graphics 620 terintegrasi untuk permainan luar biasa dan video streaming. Hdmi? Port memungkinkan Anda melihat video, foto, dan game langsung di HDTV atau layar yang lebih besar\r\nBluetooth terintegrasi 4. 0 teknologi. Memungkinkan transfer data nirkabel jarak pendek hingga 30 \'Dengan perangkat lain yang mendukung Bluetooth, termasuk speaker, printer, dan telepon. Windows 10 Home 64 Bit, Baterai: 4-cell lithium-ion, 0. 9 \"Thin, 5. 07 lbs Berat, warna: hitam\r\nLaptop bukan Drive Optik DVD','download.jpg','dell, new, laptop'),(8,3,6,'Oppo F1S',2999999,'\r\nSangat ramping & ringan namun fleksibel untuk menahan guncangan dan goresan untuk penggunaan jangka panjang.\r\nDibuat dengan Thermoplastic Poly-Urethane kualitas premium yang tahan lama dengan teknologi Anti-Slip untuk mencegah terjatuh secara tidak sengaja.\r\nLapisan bening khusus untuk ketahanan terhadap kekuningan / perubahan warna dan debu.\r\nDirancang dan diproduksi dengan cut-out & cetakan yang presisi; sangat cocok untuk perangkat Anda.\r\nOPPO F1S Case Kompatibel Hanya dengan OPPO F1S.','QMobile-Noir-i8i-Techjuice.jpg','oppo, handphone'),(12,3,4,'Samsung Galaxy S9+',6999999,'Kekuatan 5G: Dapatkan kekuatan tingkat berikutnya untuk semua yang Anda sukai dengan Samsung Galaxy 5G; Bagikan lebih banyak, game lebih keras, pengalaman lebih banyak dan jangan pernah berhenti berdetak\r\nAI Pengambilan Tunggal: Tangkap video dan beberapa jenis gambar dengan satu ketukan tombol rana; Lensa, efek, dan filter menangkap yang terbaik dari setiap momen, setiap saat\r\nZoom Kamera Hi-Res: Tangkap gambar hi-res dari jarak 300 kaki yang terlihat seperti diambil dari jarak 3 kaki; 100x Space Zoom baru yang mengubah game menghasilkan daya dan kejelasan yang belum pernah terjadi sebelumnya\r\nMode Malam Ultra Terang: Tangkap konten pro-kualitas dalam mode Malam Ultra Terang untuk mengambil foto yang mempesona, bebas kabur dan video HRD yang jelas tanpa blitz, bahkan dalam cahaya redup\r\nSuper Fast Charging: Isi daya lebih cepat dengan Super Fast Charge agar Anda dapat terus bergerak, dengan lebih banyak jus; Berikan kuncup - atau Galaxy Buds - peningkatan daya dengan Wireless PowerShare langsung dari Galaxy S20 Ultra 5G\r\nBaterai Sepanjang Hari: Baterai cerdas S20 Ultra 5G menggunakan algoritma untuk belajar dari bagaimana Anda hidup untuk mengoptimalkan daya dan membawa Anda melalui satu hari atau lebih pekerjaan dan kehidupan tanpa pernah memberi tahu Anda','samsung1.jpg','Samsung Galaxy S9+ - 6.2\" - 6GB RAM - 128GB ROM - Lilac Purple'),(13,3,4,'Samsung J6',4599900,'HSDPA 850/900/1900/2100 - LTE band 1 (2100), 2 (1900), 3 (1800), 5 (850), 7 (2600), 8 (900), 20 (800), 20 (800), 38 (2600) , 40 (2300), 41 (2500) - Dual SIM (Nano-SIM, dual stand-by)\r\n(6.2 \") HD + Infinity-V 720 x 1520 piksel, rasio 19: 9 (~ kerapatan 271 ppi) - Kaca depan, badan plastik\r\nROM 32GB + 2GB RAM - microSD, hingga 512 GB - Android Pie Samsung One UI - Exynos 7884 Octa-Core - Baterai Li-Ion 3400 mAh yang tidak dapat dilepas\r\nPonsel yang tidak terkunci kompatibel dengan operator GSM seperti AT&T dan T-Mobile, tetapi TIDAK akan bekerja dengan operator CDMA seperti Verizon dan Sprint\r\n2G bands GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2\r\n3G bands HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100\r\n4G bands LTE band 1(2100), 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 12(700), 13(700), 17(700), 20(800), 28(700), 38(2600), 40(2300), 41(2500), 66(1700/2100)\r\nSpeed HSPA, LTE\r\nGPRS Yes\r\nEDGE Yes\r\nBODY\r\nDimensions 149.3 x 70.2 x 8.2 mm (5.88 x 2.76 x 0.32 in)\r\nWeight 154 g (5.43 oz)\r\nBuild Plastic body\r\nSIM Dual SIM (Nano-SIM, dual stand-by)\r\nDISPLAY\r\nType Super AMOLED capacitive touchscreen, 16M colors\r\nSize 5.6 inches, 79.6 cm2 (~75.9% screen-to-body ratio)\r\nResolution 720 x 1480 pixels, 18.5:9 ratio (~294 ppi density)\r\nMultitouch Yes\r\nPLATFORM\r\nOS Android 8.0 (Oreo)\r\nChipset Exynos 7870 Octa\r\nCPU Octa-core 1.6 GHz Cortex-A53\r\nGPU Mali-T830 MP1\r\nMEMORY\r\nCard slot microSD, up to 256 GB\r\nInternal 32 GB, 3 GB RAM\r\nCAMERA\r\nPrimary 13 MP (f/1.9, 28mm), autofocus, LED flash\r\nFeatures Geo-tagging, touch focus, face detection, panorama, HDR\r\nVideo 1080p@30fps\r\nSecondary 8 MP, f/1.9, LED flash\r\nSOUND\r\nAlert types Vibration; MP3, WAV ringtones\r\nLoudspeaker Yes\r\n3.5mm jack Yes\r\nCONNECTIVITY\r\nWLAN Wi-Fi 802.11 b/g/n, Wi-Fi Direct, hotspot\r\nBluetooth 4.2, A2DP, LE\r\nGPS Yes, with A-GPS, GLONASS, BDS\r\nRadio FM radio\r\nUSB microUSB 2.0\r\nFEATURES\r\nSensors Fingerprint (rear-mounted), accelerometer, proximity\r\nMessaging SMS(threaded view), MMS, Email, Push Email, IM\r\nBrowser HTML5\r\nMP4/H.264 player\r\nMP3/WAV/eAAC+/FLAC player\r\nPhoto/video editor\r\nDocument viewer\r\nBATTERY\r\nNon-removable Li-Ion 3000 mAh battery\r\nTalk time Up to 21 h (3G)\r\nMusic play Up to 76 h','samsung2.jpg','Samsung J6 - 5.6\" HD+ Full View Display - 3GB RAM - 32GB ROM - 13/8Mp Camera - Gold'),(14,1,2,'DELL Inspiron 3576',4897000,'Dell 5570 15.6 \"Touchscreen FHD (1920x1080) (dukungan 10-Finger Multi-TOUCH) | Webcam HD 720p\r\nIntel Quad Core i5-8250U Generasi 8 (frekuensi dasar 1,6 GHz, hingga 3,4 GHz dengan Teknologi Intel Turbo Boost, cache 6 MB, 4 Cores 8 Threads)\r\n1 TB 5400 rpm HDD SATA | 8 GB DDR4 2400 MHz SDRAM | Intel UHD Graphics 620 Terintegrasi | Tidak ada DVD\r\nWaves MaxxAudio | HDMI | USB type-A 3.1 Gen 1 | Port Ethernet Gigabit | Wi-Fi 802.11ac + BlueTooth Combo\r\nWindows 10 Home 64-Bit; Terbaik untuk Sekolah, Rumah, Usaha Kecil, Permainan Lite.','dell1.jpg','DELL Inspiron 3576 - 15.6\" - 8th Gen. Ci5-8250U - 4GB RAM 1TB HDD - 2GB AMD Radeon - DOS'),(15,1,8,'Toshiba LAPTOP I3',3549000,'Intel CoreTM i7-5500U Processor\r\n12GB DDR3L 1600MHz memory\r\n1TB 5400 RPM, Serial ATA Hard Drive\r\n15.6 HD TruBrite LED Backlit display\r\nDVD-SuperMulti drive/ Windows 10color: #606060; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 12px;\">\r\n<li style=\"box-sizing: border-box;\">4 GB Ram</li>\r\n<li style=\"box-sizing: border-box;\">250 GB HardDisk</li>\r\n<li style=\"box-sizing: border-box;\">HIgh Graphics 15.6 Inch Led</li>\r\n<li style=\"box-sizing: border-box;\">Intel Built in Graphic Card</li>\r\n<li style=\"box-sizing: border-box;\">Intel Core i3 Processor</li>\r\n<li style=\"box-sizing: border-box;\">Plain Box</li>\r\n</ul>','toshiba.jpg','Toshiba LAPTOP I3 1st GENERATION HIGH GRAPHICS GAMING BRANDED LAPTOP NOTEBOOK- 15.6 HD LED REFURBISHED LAPTOP'),(16,2,5,'Sony Alpha a58',5450000,'20.1 MP Exmor HD APS sensor with 5 FPS shooting\r\nTranslucent Mirror Technology accelerates AF performance\r\n1080/60i/24p Full HD or 1080/30p MP4 movies w/ Quick AF\r\nLock-on AF for even easier focusing of moving subjects\r\nSVGA OLED True-Finder optimizes eye-level framing','song1.jpg','Sony Alpha a58 - DSLR Camera with 18-55mm Lens - Black (Brand Warranty)'),(17,7,4,'Samsung Original Gear S3',450000,'Gear S 3 replacement band\r\nblack silicone\r\nstylish','samsung2 (2).jpg','Samsung Original Gear S3 Classic 46 MM 4GB Rom Box packed - Silver&Black'),(18,1,11,'Rog',12535000,'Intel Core i7-9750H\r\n8 GB DDR4\r\n512 GB SSD\r\nWiFi, Bluetooth\r\nNvidia GeForce GTX 1650 4 GB\r\n15.6-inch Full HD IPS, 120 Hz, RGB\r\nWindows 10 Home','rog asus.jpg','Laptop, Gaming');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-13  5:39:26
