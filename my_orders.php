<?php
$user = $_SESSION['customer_email'];
include "includes/db_connetion.php";
function query($data)
{
    global $con;
    $result = mysqli_query($con, $data);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}
$value = query("select * from payment where pay_email='$user' ORDER BY pay_id DESC;");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text" href="/onlineshop/css/bootstrap.min.css">
    <title>Document</title>
</head>

<body>
    <h1>My Order</h1>
    <?php foreach ($value as $row) { ?>
        <table class="table" style="justify-content: left;">
            <thead>
                <tr>
                    <th>Pesanan ID : <?= $row['pay_id']; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td scope="row">Pesanan Atas Nama : <?= $row['pay_first']; ?></td>
                </tr>
                <tr>
                    <td scope="row">Total Pembayaran : Rp. <?= number_format($row['pay_total'], 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td scope="row">Status : <?php if ($row['pay_status'] == 1) {
                                                    echo "<span>sukses</span>";
                                                } else if ($row['pay_status'] == -1) {
                                                    echo "<span>rejected</span>";
                                                } else {
                                                    echo "<span>waiting</span>";
                                                } ?></td>
                    <td><a class="btn btn-primary" href="check_struk.php?id=<?= $row['pay_id']; ?>">Check Struk</a></td>
                </tr>
            </tbody>
            <hr width=100% size=3 color=black align=left>
        </table>
    <?php } ?>

</body>

</html>