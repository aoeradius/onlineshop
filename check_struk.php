<!DOCTYPE html>
<?php
session_start();
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    require "functions/functions.php";
}
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>My Online Shop</title>
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
</head>

<body>
    <div class="main_wrapper">
        <div class="header_wrapper">
            <a href="index.php"><img id="logo" src="images/logo.jpg"></a>
            <img id="banner" src="images/banner.gif">
        </div>
        <div class="menubar">
            <ul id="menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="my_account.php">My Account</a></li>
                <li><a href="customer_register.php">Sign Up</a></li>
                <li><a href="cart.php">Cart</a></li>
            </ul>
            <div id="form">
                <form method="get" action="results.php">
                    <input type="text" name="user_query" placeholder="Search products">
                    <input type="submit" name="search" value="Search">
                </form>
            </div>
        </div>
        <div class="content_wrapper">
            <div id="sidebar">
                <div class="sidebar_title">Categories </div>
                <ul class="cats">
                    <?php getCats(); ?>
                </ul>
                <div class="sidebar_title">Brands </div>
                <ul class="cats">
                    <?php getBrands(); ?>
                </ul>
            </div>
            <div id="content_area">
                <?php
                function list_pay($id)
                {
                    $query = "SELECT * FROM payment WHERE pay_id = $id";
                    return result($query);
                }


                function result($query)
                {
                    global $con;
                    if ($result = mysqli_query($con, $query)) {
                        return $result;
                    }
                }
                $data = list_pay($id);
                while ($row = mysqli_fetch_assoc($data)) { ?>
                    <input type="hidden" name="<?= $row['pay_id']; ?>" id="pay_id">
                    <h4>BIODATA PEMBELI</h4>
                    <hr width=100% size=3 color=black align=left>
                    Nama : <?= $row['pay_first']; ?><br>
                    <br>Alamat : <?= $row['pay_alamat']; ?></br>
                    <br>Email : <?= $row['pay_email']; ?></br>
                    <br>Pengiriman : <?= $row['pay_ekspedisi']; ?></br>
                    <hr width=100% size=3 color=black align=left>
                    <h4>Information Rekening Admin</h4>
                    <hr width=100% size=3 color=black align=left>
                    Transfer via : <?= $row['pay_transfer']; ?><br>
                    <br> Atas Nama : <?= $row['pay_atas_nama']; ?></br>
                    <br> No Rekening : <?= $row['pay_norek']; ?></br>
                    <hr width=100% size=3 color=black align=left>
                    <h4>Detail Pembelanjaan</h4>
                    <hr width=100% size=3 color=black align=left>


                    <?php                 // harga atau payment
                    if ($row['pay_total'] == null) {
                        echo "tidak ada transaksi";
                    } else {
                        $ip = getIp();
                        $total = 0;
                        $sel_price = "select * from cart where ip_add = '$ip'";
                        $run_price = mysqli_query($con, $sel_price);
                        while ($cart_row = mysqli_fetch_array($run_price)) {
                            $pro_id = $cart_row['p_id'];
                            $pro_qty = $cart_row['qty'];
                            $pro_price = "select * from products where pro_id = '$pro_id'";
                            $run_pro_price = mysqli_query($con, $pro_price);
                            while ($pro_row = mysqli_fetch_array($run_pro_price)) {
                                $pro_title = $pro_row['pro_title'];
                                $pro_price = $pro_row['pro_price'];
                                $pro_price_all_items = $pro_price * $pro_qty;
                                $total += $pro_price_all_items;

                    ?>
                                <tr align="center">
                                    <pre><?php echo $pro_title; ?> 
            <label>Qty: <?php echo $pro_qty; ?></label>
            <label>Harga Satuan: </label><?php echo "Rp " . number_format($pro_price, 2, ',', '.') ?>  
            <label>Total Harga Per Items: </label><?php echo "Rp " . number_format($pro_price_all_items, 2, ',', '.') . ""; ?>
            
            </pre>

                                </tr>
                    <?php
                            }
                        }
                    }
                    ?>

                    <tr align="right">
                        <hr width=100% size=3 color=black align=left>
                        <td colspan="4"><b>Sub Total:</b></td>
                        <td><?php echo "Rp " . number_format($row['pay_total'], 2, ',', '.') ?></td>

                    <?php } ?>
                    </tr>
                    </td>
                    </tr>
                    <br><br>
                    </table>
                    </form>
                    Upload Bukti Transfer Anda:
                    <form action="upload.php" method="POST" enctype="multipart/form-data">
                        <?php
                        $data = list_pay($id);
                        while ($row = mysqli_fetch_assoc($data)) { ?>
                            <pre>
                        <input type="hidden" name="pay_id" id="pay_id" value="<?= $row['pay_id']; ?>"><input type="hidden" name="total_harga" id="total_harga" value="<?= $total; ?>">
<input type="file" name="foto" id="foto"><input type="submit" value="upload" name="submit">
                        </pre>
                        <?php } ?>
                    </form>
            </div>
        </div>
    </div>
    <div id="footer">
    </div>
</body>

</html>