<?php
//sumber w3schools.com
include("includes/db_connetion.php");
$target_dir = "gallery/";
$target_file = $target_dir . basename($_FILES["foto"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if (isset($_POST["submit"])) {
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "<script>alert('Sorry, file already exists.');
        document.location='print.php';</script>";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["foto"]["size"] > 2097152) {
        echo "<script>alert('Sorry, your file is too large.');
        document.location='print.php';</script>";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif"
    ) {
        echo "<script>alert('Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
        document.location='print.php';</script>";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "<script>alert('Sorry, your file was not uploaded.');
        document.location='print.php';</script>";
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
            $nama = basename($_FILES["foto"]["name"]);
            $id = $_POST['pay_id'];
            $total = $_POST['total_harga'];
            $tgl = date("Y-m-d");
            $query = mysqli_query($con, "UPDATE payment SET pay_total = '$total', pay_image = '$nama' WHERE pay_id = '$id'") or die($con->error);
            if ($query) {
                echo '<script>alert("Transaksi Berhasil");
                document.location="index.php";</script>';
            } else {
                echo "<script>alert('Transaksi Gagal')</script>";
            }
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}
